import { ApiProvider } from './../../services/api/api';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/models/users';
import { AuthenticationService } from 'src/app/services/auth';
import { ShitImage } from 'src/app/models/shit-images';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  public userData: User;
  public AllItems: ShitImage[];
  public environment = environment.mode;
  public myToken;
  public sortOrder = 'createdAt';
  public sortDesc = true;
  public openShitID = 0;

  constructor(
    public authService: AuthenticationService,
    private router: Router,
    private api: ApiProvider,
  ) {
    this.userData = this.authService.currentUserValue;
  }

  ngOnInit() {
    this.getContent();
  }

  ionViewDidEnter() {
    this.getContent();
  }

  getContent() {
    this.api.getShitImages().subscribe(Items => {
      this.AllItems = Items;
    });
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getContent();
      event.target.complete();
    });
  }

  openItemDetail(content?) {
    const itemID = content ? content.id : '';
    this.router.navigateByUrl('/shit-images-detail/' + itemID);
  }

}
