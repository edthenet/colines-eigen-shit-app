import { Component, OnInit } from '@angular/core';
// import { Contacts, Contact, ContactField, ContactName, ContactAddress, ContactOrganization } from '@ionic-native/contacts/ngx';
import { StaticInformationProvider } from 'src/app/services/static-information/static-information';

@Component({
  selector: 'app-about-scorpion',
  templateUrl: './about-scorpion.page.html',
  styleUrls: ['./about-scorpion.page.scss'],
})
export class AboutScorpionPage implements OnInit {

  public mailBody;
  public appVersion: string;
  public appName: string;

  constructor(
    private staticInfo: StaticInformationProvider,
  ) {
    this.appName = this.staticInfo.getAppName();
    this.appVersion = this.staticInfo.getAppVersion();
  }

  ngOnInit() {
  }

  // saveContact() {
  //   const contact: Contact = this.contacts.create();

  //   contact.displayName = 'Scorpion Computers & Software';
  //   contact.name = new ContactName(null, 'Witlox', 'Edwin');
  //   // contact.displayName = "Testing";
  //   // contact.name = new ContactName(null, 'Tester', 'Tinus');
  //   contact.phoneNumbers = [new ContactField('mobile', '+31613493685', true)];
  //   contact.emails = [new ContactField('work', 'info@scorpioncomputers.nl')];
  //   contact.birthday = new Date('1971-07-22T12:00:00.000Z');
  //   contact.urls = [new ContactField('work', 'https://scorpioncomputers.nl')];
  //   contact.photos = [new ContactField('work', 'https://scorpioncomputers.nl/images/scorpion/Logo_ScorpionComputers_350.png')];
  //   contact.note = 'Dit bedrijf maakt website en mobiele apps. Zij zijn o.a. de makers van de ' + this.appName;
  //   contact.organizations = [new ContactOrganization('Company', 'Scorpion Computers & Software', 'Development', 'CEO', true)];
  //   contact.addresses =  [new ContactAddress(false, 'work', 'Post adres', 'Goudsbloem 52', 'Udenhout', 'Udenhout', '5071 EZ', 'Nederland')];
  //   contact.addresses = [new ContactAddress(true, 'work', 'Bezoek adres', 'Kapelanijstraat 1', 'Pelt', 'Pelt', '3900', 'Belgium')];

  //   contact.save().then(
  //       () => {
  //         console.log('Contactgegevens opgeslagen!', contact);
  //         this.presentToast('Scorpion Computers & Software is opgeslagen in uw adresboek.');
  //       },
  //       (error: any) => {
  //         console.error('Fout bij het opslaan van het contact.', error);
  //         this.presentToast('Er is een fout  opgetreden' + error);
  //       }
  //   );
  // }


}
