import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
// import { Contacts, Contact, ContactField, ContactName, ContactAddress, ContactOrganization } from '@ionic-native/contacts/ngx';

import { IonicModule } from '@ionic/angular';

import { AboutScorpionPage } from './about-scorpion.page';

const routes: Routes = [
  {
    path: '',
    component: AboutScorpionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    // Contacts, Contact, ContactField, ContactName, ContactAddress, ContactOrganization
  ],
  declarations: [AboutScorpionPage]
})
export class AboutScorpionPageModule {}
