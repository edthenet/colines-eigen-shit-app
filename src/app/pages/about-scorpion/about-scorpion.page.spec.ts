import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutScorpionPage } from './about-scorpion.page';

describe('AboutScorpionPage', () => {
  let component: AboutScorpionPage;
  let fixture: ComponentFixture<AboutScorpionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutScorpionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutScorpionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
