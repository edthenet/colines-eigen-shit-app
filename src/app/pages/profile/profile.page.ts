import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/users';
import { AuthenticationService } from 'src/app/services/auth';
import { ApiProvider } from 'src/app/services/api/api';
import { UtilityService } from 'src/app/services/utility/utility.service';
import { NavController } from '@ionic/angular';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  currentUser: User;
  password: string;

  constructor(
    private authService: AuthenticationService,
    private api: ApiProvider,
    private utils: UtilityService,
    private navCtrl: NavController,
    private dialogs: Dialogs
  ) { }

  ngOnInit() {
    this.currentUser = this.authService.currentUserValue;
  }

  saveContent(currentUser: User) {
    console.log('updating');
    this.api.updateUser(currentUser).subscribe(updatedUser => {
      this.utils.presentToast('Gegevens zijn opgeslagen');
      this.navCtrl.pop();
    }, err => {
      console.warn('dat ging niet goed', err);
    });
  }

  resetPassword() {
    this.dialogs.confirm('Weet je zeker dat je dit wachtwoord wil veranderen?', 'Bevestig', ['Bevestig', 'Annuleer']).then(buttonIndex => {
      if (buttonIndex === 1) {
        this.api.changeUserPassword(this.currentUser.id, this.password).subscribe(res => {
          this.utils.presentToast('Wachtwoord gerest naar: ' + this.password);
        });
      }
    });
  }

  getAvatarURL() {
    if (this.currentUser && this.currentUser.avatar && this.currentUser.avatar.length > 1) {
      return environment.serverUrl + '/' +
        environment.api_version + '/files/avatars/' +
        this.currentUser.avatar + '?auth=' +
        this.authService.currentUserValue.token;
    } else {
      // return '/assets/imgs/profile_image.png';
      return '/assets/imgs/logo.png';
    }
  }
}
