import { Component, OnInit } from '@angular/core';
import { ApiProvider } from 'src/app/services/api/api';
import { UtilityService } from 'src/app/services/utility/utility.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/auth';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { User } from 'src/app/models/users';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  public AllItems: User[];

  constructor(
    private api: ApiProvider,
    private router: Router,
    private dialogs: Dialogs,
    public _utils: UtilityService,
    public authService: AuthenticationService,
  ) { }

  ngOnInit() {
    this.getContent();
  }

  ionViewDidEnter() {
    this.getContent();
  }

  getContent() {
    this.api.getUsers().subscribe(Items => {
      this.AllItems = Items;
    });
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getContent();
      event.target.complete();
    });
  }

  openItemDetail(content?) {
    // this.navCtrl.push(DetailPage, { content });
    const itemID = content ? content.id : '';
    this.router.navigateByUrl('/user-detail/' + itemID);
  }

  deleteItem(Item) {
    this.dialogs.confirm('Weet je zeker dat je dit item wil verwijderen?', 'Bevestig', ['Verwijder', 'Annuleer']).then(buttonIndex => {
      if (buttonIndex === 1) {
        this.api.deleteUser(Item.id).subscribe(res => {
          this._utils.presentToast('Item is verwijderd');
          this.getContent();
        });
      }
    });
  }

  startChat(chatPartner: User) {
    // this.navCtrl.push(MessagesPage, { user: chatPartner });
    this.router.navigate(['/home']);
  }

  getAvatarURL(user) {
    if (user && user.avatar && user.avatar.length > 1) {
      return environment.serverUrl + '/' +
        environment.api_version + '/files/avatars/' +
        user.avatar + '?auth=' +
        this.authService.currentUserValue.token;
    } else {
      return '/assets/imgs/profile_image.png';
    }
  }

  changeState(user: User) {
    console.log('Changing state from ', +user.isActive, ' to ', +!user.isActive);
    user.isActive = !user.isActive;
    // this.api.updateUser(user);
    this.api.updateUser(user).subscribe(updatedUser => {
      // console.log(updatedUser);
      // this.navCtrl.pop();
    }, err => {
      console.warn('dat ging niet goed', err);
    });
  }

}
