import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/users';
import { StaticInformationProvider } from 'src/app/services/static-information/static-information';
import { ApiProvider } from 'src/app/services/api/api';
import { UtilityService } from 'src/app/services/utility/utility.service';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { AuthenticationService } from 'src/app/services/auth';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.page.html',
  styleUrls: ['./user-detail.page.scss'],
})
export class UserDetailPage implements OnInit {

  user: User;
  userID;
  roles;
  password: string;

  constructor(
    private staticInformation: StaticInformationProvider,
    private api: ApiProvider,
    private utils: UtilityService,
    private dialogs: Dialogs,
    private route: ActivatedRoute,
    private navCtrl: NavController,
    public _auth: AuthenticationService,
  ) {
    this.roles = this.staticInformation.getRoles();
  }

  ngOnInit() {
    this.userID = this.route.snapshot.paramMap.get('id');
    console.log('ID:', this.userID);

    if (!this.userID) {
      this.user = new User();
      this.user.firstname = '';
    } else {
      this.getContent(this.userID);
    }
  }

  getContent(id) {
    this.api.getUserData(id).subscribe(Result => {
      // console.log(Result);
      this.user = Result;
      // console.log(this.user);
    });
  }

  saveContent(currentUser: User) {
    if (currentUser.id) {
      console.log('updating');
      this.api.updateUser(currentUser).subscribe(updatedUser => {
        this.utils.presentToast('Gegevens zijn opgeslagen');
        this.navCtrl.pop();
      }, err => {
        console.warn('dat ging niet goed', err);
      });
    } else {
      console.log('Adding new record');
      currentUser.password = this.password;
      this.api.addUser(currentUser).subscribe(newUser => {
        this.utils.presentToast('Gegevens zijn opgeslagen');
        this.navCtrl.pop();
      }, err => {
        console.warn('dat ging niet goed', err);
      });
    }
  }

  resetPassword() {
    this.dialogs.confirm('Weet je zeker dat je dit wachtwoord wil veranderen?', 'Bevestig', ['Bevestig', 'Annuleer']).then(buttonIndex => {
      if (buttonIndex === 1) {
        this.api.changeUserPassword(this.user.id, this.password).subscribe(res => {
          this.utils.presentToast('Wachtwoord gerest naar: ' + this.password);
        });
      }
    });
  }

}
