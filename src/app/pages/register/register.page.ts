import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MenuController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/auth';
import { first } from 'rxjs/operators';
import { User } from 'src/app/models/users';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private menuCtrl: MenuController,
    private authService: AuthenticationService,
    private router: Router,
  ) { }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      emailPrivate: ['', Validators.required],
      password: ['', Validators.required],
      passwordCheck: [''],
    });
  }

  get f() { return this.registerForm.controls; }

  registerUser() {
    const newUser: User = new User;
    newUser.firstname = this.f.firstname.value;
    newUser.lastname = this.f.lastname.value;
    newUser.emailPrivate = this.f.emailPrivate.value;
    newUser.password = this.f.password.value;

    this.authService.registerUser(newUser)
    .pipe(first())
    .subscribe(
        data => {
          // this.menuCtrl.enable(true);
          this.router.navigateByUrl('/login');
        },
        error => {
            // this.error = error;
            // this.loading = false;
        });
  }


}
