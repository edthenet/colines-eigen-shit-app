import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { InfoTabsPage } from './info-tabs.page';
import { InfoTabsPageRoutingModule } from './info-tabs-routing.modules';

const routes: Routes = [
  {
    path: '',
    component: InfoTabsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    InfoTabsPageRoutingModule
  ],
  declarations: [InfoTabsPage]
})
export class InfoTabsPageModule {}
