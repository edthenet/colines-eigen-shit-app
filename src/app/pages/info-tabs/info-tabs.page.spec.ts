import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoTabsPage } from './info-tabs.page';

describe('InfoTabsPage', () => {
  let component: InfoTabsPage;
  let fixture: ComponentFixture<InfoTabsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoTabsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoTabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
