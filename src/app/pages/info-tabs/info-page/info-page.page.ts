import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiProvider } from 'src/app/services/api/api';
import { BasicContent } from 'src/app/models/basiccontents';

@Component({
  selector: 'app-info-page',
  templateUrl: './info-page.page.html',
  styleUrls: ['./info-page.page.scss'],
})
export class InfoPagePage implements OnInit {

  public itemLabel = '';
  public currentItem: BasicContent;

  constructor(
    private route: ActivatedRoute,
    private api: ApiProvider
  ) {
    this.itemLabel = this.route.snapshot.paramMap.get('label');
    // console.log('Label:', this.itemLabel);
   }

  ngOnInit() {
    this.getContent(this.itemLabel);
  }

  getContent(id) {
    this.api.getBasicContentItem(id).subscribe(Result => {
      // console.log(Result);
      this.currentItem = Result;
      // console.log(this.currentItem);
    });
  }

}
