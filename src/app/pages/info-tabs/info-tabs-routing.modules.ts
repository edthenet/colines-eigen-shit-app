import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InfoTabsPage } from './info-tabs.page';

const routes: Routes = [
  {
    path: '',
    component: InfoTabsPage,
    children:
      [
        {
          path: 'tab1/:label',
          children:
          [
            {
              path: '',
              // resolve: { label: 'shitapp-releasenotes' },
              loadChildren: './info-page/info-page.module#InfoPagePageModule'
            }
          ]
        },
        {
          path: 'tab2/:label',
          children:
          [
            {
              path: '',
              // resolve: { label: 'shitapp-about' },
              loadChildren: './info-page/info-page.module#InfoPagePageModule'
            }
          ]
        },
        {
          path: 'tab3',
          children:
            [
              {
                path: '',
                loadChildren: '../../pages/about-scorpion/about-scorpion.module#AboutScorpionPageModule'
              }
            ]
        },
        {
          path: '',
          redirectTo: '/info-tabs/tab1/shitapp-about',
          pathMatch: 'full'
        }
      ]
  },
  {
    path: '',
    redirectTo: '/info-tabs/tab1/shitapp-about',
    pathMatch: 'full'
  }
];

@NgModule({
  imports:
    [
      RouterModule.forChild(routes)
    ],
  exports:
    [
      RouterModule
    ]
})
export class InfoTabsPageRoutingModule {}
