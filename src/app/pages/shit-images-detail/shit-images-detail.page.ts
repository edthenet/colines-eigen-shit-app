import { ShitImage } from 'src/app/models/shit-images';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiProvider } from 'src/app/services/api/api';
import { UtilityService } from 'src/app/services/utility/utility.service';
import { NavController } from '@ionic/angular';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { AuthenticationService } from 'src/app/services/auth';

@Component({
  selector: 'app-shit-images-detail',
  templateUrl: './shit-images-detail.page.html',
  styleUrls: ['./shit-images-detail.page.scss'],
})
export class ShitImagesDetailPage implements OnInit {

  currentItem;
  itemID;
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    uploadUrl: 'v1/images', // if needed
    customClasses: [ // optional
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ]
  };

  constructor(
    private api: ApiProvider,
    private utils: UtilityService,
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private authService: AuthenticationService,

  ) { }

  ngOnInit() {
    this.itemID = this.route.snapshot.paramMap.get('id');
    console.log('ID:', this.itemID);

    if (!this.itemID) {
      this.currentItem = new ShitImage();
      this.currentItem.senderId = this.authService.currentUserValue.id;
    } else {
      this.getContent(this.itemID);
    }
  }

  getContent(id) {
    this.api.getShitImagesItem(id).subscribe(Result => {
      this.currentItem = Result;
    });
  }

  saveContent(currentItem) {
    if (currentItem.id) {
      console.log('updating');
      this.api.updateShitImagesItem(currentItem).subscribe(updatedItem => {
        this.utils.presentToast('Gegevens zijn opgeslagen');
        this.navCtrl.navigateBack('/home');
      }, err => {
        console.warn('dat ging niet goed', err);
      });
    } else {
      console.log('Adding new record');
      this.api.addShitImagesItem(currentItem).subscribe(newItem => {
        this.utils.presentToast('Gegevens zijn opgeslagen');
        this.navCtrl.navigateBack('/home');
      }, err => {
        console.warn('dat ging niet goed', err);
      });
    }
  }

}
