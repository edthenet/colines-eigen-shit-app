import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/auth';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { Subscription } from 'rxjs';
import { FingerprintAIO, FingerprintOptions } from '@ionic-native/fingerprint-aio/ngx';
import { Platform } from '@ionic/angular';
import { StaticInformationProvider } from '../../services/static-information/static-information';
import { FingerprintService } from 'src/app/services/fingerprint.service';
import { FirebaseFCMService } from 'src/app/services/firebase-fcm.service';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { LoggingService } from '../../services/logging.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginPage implements OnInit, OnDestroy {

  private subscriptionFingerprint: Subscription;

  public loginForm: FormGroup;
  public formSubmitted = false;
  public showRememberMe = true;
  public errorMessage;

  public fingerprintAvailable = false; // is fingerprint available on this device
  public fingerprintUse = false; // Does the user want to use fingerprint
  public fingerprintFirstTimeQuestion = false; // Is this the first time we use this device with fingerprint
  public fingerPrintRetry = false;
  public myToken;

  public loginInfo = {
    username: '',
    wachtwoord: '',
    rememberMe: false
  };

  constructor(
    private router: Router,
    private faio: FingerprintAIO,
    private dialogs: Dialogs,
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    private staticInfo: StaticInformationProvider,
    public fingerprintSrv: FingerprintService,
    private fcm: FirebaseFCMService,
    private platform: Platform,
    private localNotifications: LocalNotifications,
    private loggingService: LoggingService,
  ) { }

  ngOnInit() {
    // Check is data is available in local storage
    this.getLocalStorageValues();
    // Check if fingerprint/faceid is available
    this.subscriptionFingerprint = this.fingerprintSrv.getFingerprintIsAvailable().subscribe(fpAvailable => {
      this.fingerprintAvailable = fpAvailable;
      this.requestFingerprint();
    });
    // Then we can build the form (if data was available the form will use these)
    this.buildForm();
  }

  ngOnDestroy() {
    if (this.subscriptionFingerprint) { this.subscriptionFingerprint.unsubscribe(); }
  }

  buildForm() {
    // Fil the form with the available data (if rememberMe was checked before)
    this.loginForm = this.formBuilder.group({
      username: [this.loginInfo.rememberMe ? this.loginInfo.username : '', Validators.compose([Validators.required, Validators.email])],
      wachtwoord: [this.loginInfo.rememberMe ? this.loginInfo.wachtwoord : '', Validators.required],
      rememberMe: [this.loginInfo.rememberMe]
    });
  }
  get f() { return this.loginForm.controls; }

  getLocalStorageValues() {
    // We first fill a const to check if the data is available
    const fpUse = localStorage.getItem(`${this.staticInfo.getAppPrefixDB()}fingerprintUse`);
    if (fpUse !== 'yes' && fpUse !== 'no') {
      this.fingerprintFirstTimeQuestion = true;
    } else {
      this.fingerprintUse = fpUse === 'yes';
    }
    const lInfo = localStorage.getItem(`${this.staticInfo.getAppPrefixDB()}loginInfo`);
    if (lInfo) { this.loginInfo = JSON.parse(lInfo); }
  }

  requestFingerprint() {
    // First we check if fingerprint is available on the device
    if (this.fingerprintAvailable) {
      this.loggingService.info('Fingerprint of FaceID seems to be available');
      if (this.fingerprintUse) {
        this.loggingService.info('The user has selected to use the fingerprint of FaceID');

        this.loginForm.controls.rememberMe.setValue(true);
        this.loginForm.controls.username.setValue('');
        this.loginForm.controls.wachtwoord.setValue('');

        const fpOption: FingerprintOptions = {
          description: 'Log in met uw vingerafdruk of gezichtsherkenning',
          disableBackup: true,  // Only for Android(optional)
        };

        this.faio.show(fpOption).then((result: any) => {
          this.loginForm.controls.rememberMe.setValue(true);
          this.loginForm.controls.username.setValue(this.loginInfo.username);
          this.loginForm.controls.wachtwoord.setValue(this.loginInfo.wachtwoord);
          // And then we run the login function
          this.doLogin();
        }).catch((error: any) => {
          this.loggingService.error('Error for fingerprint', error);
          this.fingerPrintRetry = true;
        });
      }
    } else {
      this.loggingService.warn('Fingerprint does not seem te be available at this time.');
    }
  }

  async doLogin() {
    this.formSubmitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    if (this.fingerprintAvailable && this.fingerprintFirstTimeQuestion) {
      const buttonClicked = await this.dialogs.confirm('Wil je de volgende keer met je vingerafdruk inloggen?', 'Vingerafdruk', ['Ja', 'Nee']);
      if (buttonClicked === 1) {
        this.fingerprintUse = true;
      } else {
        this.fingerprintUse = false;
      }
      this.loggingService.info('fingerprintUse', this.fingerprintUse ? 'yes' : 'no');
    } else {
      // this.loggingService.log("Not buttonclicked");
    }

    this.authService.login(this.f.username.value, this.f.wachtwoord.value).subscribe(data => {
      // We save the data to local storage for later use (security risk accepted by customer)
      localStorage.setItem(`${this.staticInfo.getAppPrefixDB()}fingerprintUse`, this.fingerprintUse ? 'yes' : 'no');
      localStorage.setItem(`${this.staticInfo.getAppPrefixDB()}loginInfo`, JSON.stringify(this.loginForm.getRawValue()));
      this.prepareForPushMessaging();
      this.router.navigate(['/home']);
    }, error => {
      this.errorMessage = error;
    });
  }

  prepareForPushMessaging() {
    this.loggingService.info('PREPARE FOR PUSH MESSAGING');
    if (this.platform.is('ios') || this.platform.is('android')) {
      // Get a FCM token
      this.loggingService.info('RETRIEVE A TOKEN BEFORE WE CAN CONTINUE');
      this.fcm.getToken().then(token => {
        // Nu vastzetten om op het beeld te tonen
        this.myToken = this.fcm.getMyToken();

        this.fcm.registerDevice()
          .subscribe(result => {
            // Listen to incoming messages
            this.fcm.listenToNotifications().subscribe(msg => {
              this.loggingService.info('NOTIFICATION!!', JSON.stringify(msg));
              let prefix;

              if (msg.tap) {
                // If push is from a chatsession, open the specific chat session directly
                if (msg.sender) {
                  this.router.navigate(['chat-session/', msg.sender]);
                }
              } else {
                prefix = 'Pushbericht: ';
                // show a local Notification (with fix for iOS different content from API and Firebase Console)
                const msgTitle = (msg.aps && msg.aps.alert) ? msg.aps.alert.title : msg.title;
                const msgBody = (msg.aps && msg.aps.alert) ? msg.aps.alert.body : msg.body;

                this.localNotifications.schedule({
                  id: Math.round(Math.random() * 10000),
                  title: msgTitle,
                  data: { msg },
                  foreground: true,
                  text: msgBody,
                  priority: 10,
                  color: '#00AFEC',
                });
              }
            });
          });
      }, err => this.loggingService.warn(err));
      this.localNotifications.on('click').subscribe(n => {
        if (n.data.msg && n.data.msg.sender) {
          this.router.navigate(['chat-session/', n.data.msg.sender]);
        }
      });
    }
  }
}
