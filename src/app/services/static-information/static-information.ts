import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StaticInformationProvider {

  private phoneNumberHQ = '+31850609555';
  private appVersion = '3.0.0';
  private appName = 'Coline`s eigen shit app';
  private roles: string[] = ['admin', 'manager', 'user'];
  private appPrefixDB = 'scorpion-shitapp-';
  private appid = 'com.scorpion.colinesshitapp';

  constructor() {}

  getPhoneNumberHQ() {
    return this.phoneNumberHQ;
  }

  getAppVersion() {
    return this.appVersion;
  }

  getAppName() {
    return this.appName;
  }

  getRoles() {
    return this.roles;
  }

  isMinimumRole(usersRole, minRole) {
    const mri = this.roles.findIndex(mrix => mrix === minRole);
    const uri = this.roles.findIndex(urix => urix === usersRole);
    return mri <= uri;
  }

  getAppPrefixDB() {
    return this.appPrefixDB;
  }

  getAppID() {
    return this.appid;
  }
}
