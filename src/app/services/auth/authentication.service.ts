﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from 'src/app/models/users';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    private storagePrefix = 'scorpion-shitapp-';

    constructor(
        private http: HttpClient,
        private router: Router,
    ) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem(`${this.storagePrefix}currentUser`)));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        return this.http.post<any>(`${environment.serverUrl}/${environment.api_version}/users/login`, { email: username, password: password })
            .pipe(map(result => {
                // login successful if there's a jwt token in the response
                // console.log(result.result);

                if (result.result.user && result.result.token) {
                    result.result.user.token = result.result.token;
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem(`${this.storagePrefix}currentUser`, JSON.stringify(result.result.user));
                    this.currentUserSubject.next(result.result.user);
                }

                return result.result.user;
            }));
    }

    registerUser(userData: User) {
        return this.http.post<any>(`${environment.serverUrl}/${environment.api_version}/users/register`,  userData )
            .pipe(map(result => {
                // login successful if there's a jwt token in the response
                // console.log(result.result);

                if (result.result.user && result.result.token) {
                    result.result.user.token = result.result.token;
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem(`${this.storagePrefix}currentUser`, JSON.stringify(result.result.user));
                    this.currentUserSubject.next(result.result.user);
                }

                return result.result.user;
            }));
    }

    logout() {
        console.log('Loging out...');
        // remove user from local storage to log user out
        localStorage.removeItem(`${this.storagePrefix}currentUser`);
        this.currentUserSubject.next(null);
        this.router.navigate(['/login']);
    }
}
