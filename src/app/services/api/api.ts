import { JoomlaArticle } from './../../models/joomla-article';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ApiProvider {

  cachedRequests: Array<HttpRequest<any>> = [];
  currentJoomlaArticle: JoomlaArticle;

  constructor(
    public http: HttpClient,
  ) {
    // console.log('Hello ApiProvider Provider');
  }

  getTest() {
    return this.doGetRequest('/');
  }

  getProtectedTest() {
    return this.doGetRequest('/protected');
  }

  // Users
  getUsers(queryParams?) {
    return this.doGetRequest('/users', undefined, queryParams);
  }

  updateUser(user) {
    return this.doPutRequest('/users/' + user.id, user);
  }

  addUser(data) {
    return this.doPostRequest('/users/register', data);
  }

  deleteUser(userId) {
    return this.doDeleteRequest('/users/' + userId);
  }

  resetPassword(token, newPassword) {
    return this.doPostRequest('/users/resetpassword', {token, newPassword});
  }

  requestNewPassword(email) {
    return this.doPostRequest('/users/resetpassword', {email});
  }

  getUserData(id) {
    return this.doGetRequest(`/users/${id}`);
  }

  getMyUserData() {
    return this.doGetRequest('/users/me');
  }

  changePassword(oldpass, newpass) {
    return this.doPutRequest('/users/password/', {
      currentPassword: oldpass,
      newPassword: newpass
    });
  }

  changeUserPassword(userid, newpass) {
    return this.doPutRequest('/users/' + userid + '/password/', {
      newPassword: newpass
    });
  }

  registerDevice(data) {
    return this.doPostRequest('/user_devices/', data);
  }

  // Allerlei shit
  getShitImages(queryParams?) {
    return this.doGetRequest('/shit-images/', undefined, queryParams);
  }

  getShitImagesFromJoomla() {
    const queryParams = { 'catid': '9', 'orderdir': 'asc' };
    return this.doGetRequestFromJoomla('https://apps.scorpionsoftware.nl/rest/get/content/articles', queryParams);
  }

  getShitImagesItem(id, queryParams?) {
    return this.doGetRequest(`/shit-images/${id}`, undefined, queryParams);
  }

  addShitImagesItem(Item) {
    return this.doPostRequest('/shit-images', Item);
  }

  updateShitImagesItem(Item) {
    return this.doPutRequest('/shit-images/' + Item.id, Item);
  }

  deleteShitImagesItem(id) {
    return this.doDeleteRequest('/neshit-imagesws/' + id);
  }

  // Basic Content
  getBasicContents(queryParams?) {
    return this.doGetRequest('/basic_contents', undefined, queryParams);
  }

  getBasicContentItem(label, queryParams?) {
    return this.doGetRequest(`/basic_contents/${label}`, undefined, queryParams);
  }

  addBasicContent(Item) {
    return this.doPostRequest('/basic_contents', Item);
  }

  updateBasicContent(content) {
    return this.doPutRequest('/basic_contents/' + content.id, content);
  }

  // Messages
  getMessages(partnerId) {
    return this.doGetRequest('/messages/' + partnerId);
  }

  sendMessage(message) {
    return this.doPostRequest('/messages', message);
  }

  sendHeadquartersMessage(message) {
    return this.doPostRequest('/messages/headquarters', message);
  }

  getChats() {
    return this.doGetRequest('/messages');
  }

  getHeadQuartersChats() {
    return this.doGetRequest('/messages/headquarters');
  }

  getHeadQuartersMessages(partnerId) {
    return this.doGetRequest('/messages/headquarters/' + partnerId);
  }

  setMessagesRead(receiverId, senderId) {
    return this.doPutRequest('/messages', { isRead: true }, undefined, { senderId, receiverId });
  }

  // News From Joomla
  getNewsFromJoomla(typeOfNews?, queryParams?) {
    if ( !typeOfNews ) { typeOfNews = 'latest' ; } // Andere opties zijn 'all' en 'technical'
    return this.doGetRequest('/joomla_contents/scorpionnews/' + typeOfNews, undefined, queryParams);
  }

  setCurrentJoomlaArticle(item) {
    this.currentJoomlaArticle = item;
  }

  getCurrentJoomlaArticle() {
    return this.currentJoomlaArticle;
  }


  /**
 * @desc Do Get Requests to the API-Gateway Server
 */
  doGetRequest(endPoint: string, headersValues?: Object, queryParams?) {
    // console.log('GET request naar ', environment.serverUrl + '/' + environment.api_version + endPoint);
    let headers = new HttpHeaders();
    if (headersValues) {
      const keys = Object.keys(headersValues);
      keys.forEach(key => {
        headers = headers.set(key, String(headersValues[key]));
      });
    }

    const urlSearchParams = new URLSearchParams();
    // console.log(queryParams);
    if (queryParams) {
      Object.keys(queryParams).forEach(key => {
        // console.log(key);
        // console.log(queryParams[key]);
        urlSearchParams.append(key, queryParams[key]);
      });
    }

    return this.http.get(environment.serverUrl + '/' + environment.api_version + endPoint + '?' + urlSearchParams.toString(), { headers: headers }).pipe(map((result: any) => {
      return result;
    }));
  }

  /**
 * @desc Do Get Requests to the API-Gateway Server
 */
  doGetRequestFromJoomla(endPoint: string, queryParams?) {
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json');

    const urlSearchParams = new URLSearchParams();
    if (queryParams) {
      Object.keys(queryParams).forEach(key => {
        urlSearchParams.append(key, queryParams[key]);
      });
    }

    return this.http.get(endPoint + '?' + urlSearchParams.toString(), { headers: headers }).pipe(map((result: any) => {
      return result;
    }));
  }

  /**
   * @desc Do a PUT to the Server
   */
  doPutRequest(endPoint: string, data: Object, headersValues?: Object, queryParams?) {
    // console.log('PUT request naar ', environment.serverUrl + endPoint);

    let headers = new HttpHeaders();
    if (headersValues) {
      const keys = Object.keys(headersValues);
      keys.forEach(key => {
        headers = headers.set(key, String(headersValues[key]));
      });

    }


    const urlSearchParams = new URLSearchParams();
    // console.log(queryParams);
    if (queryParams) {
      Object.keys(queryParams).forEach(key => {
        // console.log(key);
        // console.log(queryParams[key]);
        urlSearchParams.append(key, queryParams[key]);
      });
    }

    // console.log(urlSearchParams);

    // console.log(environment.serverUrl + endPoint, data);
    return this.http.put(environment.serverUrl + '/' + environment.api_version + endPoint + (queryParams && Object.keys(queryParams).length > 0 ? '?' + urlSearchParams.toString() : ''), data).pipe(map((result: any) => {
      return result;
    }));
  }

  /**
   * @desc Do a POST to the Server
   */
  doPostRequest(endPoint: string, data: Object, headersValues?: Object) {
    // console.log('POST request naar ', environment.serverUrl + endPoint);

    let headers = new HttpHeaders();
    if (headersValues) {
      const keys = Object.keys(headersValues);
      keys.forEach(key => {
        headers = headers.set(key, String(headersValues[key]));
      });

    }
    return this.http.post(environment.serverUrl + '/' + environment.api_version + endPoint, data).pipe(map((result: any) => {
      return result;
    }));
  }

  /**
   * @desc Do Delete Requests to the API-Gateway Server
   */
  doDeleteRequest(endPoint: string, headersValues?: Object) {
    // console.log('DELETE request naar ', environment.serverUrl + '/' + environment.api_version + endPoint);
    let headers = new HttpHeaders();
    if (headersValues) {
      const keys = Object.keys(headersValues);
      keys.forEach(key => {
        headers = headers.set(key, String(headersValues[key]));
      });

    }

    return this.http.delete(environment.serverUrl + '/' + environment.api_version + endPoint, { headers: headers }).pipe(map((result: any) => {
      return result;
    }));
  }

  // doFileUpload(fileKey, filePath, endPoint) {
  //   return new Promise((resolve,reject)=>{
  //     const fileTransfer: FileTransferObject = this.transfer.create();
  //     this._ss.getJSONKey('userdata').then(result => {
  //       let token = (result as any).token;
  //       let options: FileUploadOptions = {
  //         httpMethod:"PUT",
  //         fileKey,
  //         headers: {
  //         'Authorization': 'Bearer ' + token,
  //         }
  //       }
  //       fileTransfer.upload(filePath, this._config.config.serverUrl + '/' + this._config.config.api_version + endPoint, options)
  //        .then((data) => {
  //          resolve(data);
  //        }, (err) => {
  //          reject(err)
  //        })
  //     },error => reject(error));

  //   })
  // }


}
