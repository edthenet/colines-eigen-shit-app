// import { Injectable } from '@angular/core';
// import io from 'socket.io-client';
// import { Observable } from 'rxjs';
// import { AuthenticationProvider } from '../authentication/authentication';
// import { ENV } from '@app/env';

// @Injectable({ providedIn: 'root' })
// export class SocketProvider {

//     socket;

//     constructor(
//         private auth: AuthenticationProvider
//     ) {
//         console.log(ENV.serverUrl);
//         this.socket = io(ENV.serverUrl).connect();

//         this.socket.on('connect', () => {
//             console.log('socket is connected!');
//             this.socket.emit("add-user", this.auth.getUserData().id);
//         });
//     }

//     on(event) {
//         return Observable.create(observer => this.socket.on(event, result => observer.next(result)));
//     }

//     join(room){
//         this.socket.emit("subscribe", { room });
//     }

//     leave(room){
//         this.socket.emit("unsubscribe", { room });
//     }
// }
