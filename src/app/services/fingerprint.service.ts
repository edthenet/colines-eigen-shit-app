import { Injectable } from '@angular/core';
import { FingerprintAIO, FingerprintOptions } from '@ionic-native/fingerprint-aio/ngx';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FingerprintService {

  private fingerPrintAvailable = new Subject<boolean>();

  private isFingerprintAvailable;

  constructor(
    private faio: FingerprintAIO
  ) {
    this.checkIfFingerprintIsAvailable();
  }

  checkIfFingerprintIsAvailable() {
    console.info('Check with plugin if Fingerprint or FaceID is avaiable');
    this.faio.isAvailable().then(result => {
      console.info('fingerprintIsAvailable Result: ', result);
      if (result === 'finger' || result === 'face' || result === 'biometric') {
        console.info('So this function wil give back as result:', result === 'finger' || result === 'face' || result === 'biometric');
        this.fingerPrintAvailable.next(true);
      } else {
        this.fingerPrintAvailable.next(false);
      }
    }).catch(err => {
      console.error('fingerprintIsAvailable Error: ', err);
      this.fingerPrintAvailable.next(false);
    });
  }

  getFingerprintIsAvailable(): Observable<boolean> {
    return this.fingerPrintAvailable.asObservable();
  }

  fingerPrintIsAvailable(): Promise<boolean> {
    return new Promise((resolve, reject) => {

      if (typeof this.isFingerprintAvailable !== 'undefined') {
        resolve(this.isFingerprintAvailable);
      } else {
        this.faio.isAvailable().then(result => {
          console.info('fingerprintIsAvailable Result: ', result);
          if (result === 'finger' || result === 'face' || result === 'biometric') {
            console.info('So this function wil give back as result:', result === 'finger' || result === 'face' || result === 'biometric');
            // this.fingerPrintAvailable.next(true);
            this.isFingerprintAvailable = true;
          } else {
            this.isFingerprintAvailable = false;
            // this.fingerPrintAvailable.next(false);
          }
          resolve(this.isFingerprintAvailable);

        }).catch(err => {
          console.error('fingerprintIsAvailable Error: ', err);
          reject(err);
          // this.fingerPrintAvailable.next(false);
        });
      }

    });

  }

  loginWithFingerprint() {
    console.info('Realy logging in with fingerprint');
    let res = false;
    const fpOption: FingerprintOptions = {
      description: 'Log in met uw vingerafdruk of gezichtsherkenning',
      // clientId: 'Fingerprint-Wijrijden', // Android: Used for encryption. iOS: used for dialogue if no `localizedReason` is given.
      // clientSecret: 'o7aoOMYUbyxaD23oFAned', // Necessary for Android encrpytion of keys. Use random secret key.
      disableBackup: true,  // Only for Android(optional)
      // localizedFallbackTitle: 'Use Pin', // Only for iOS
      // localizedReason: 'Please authenticate' // Only for iOS
    };

    this.faio.show(fpOption).then((result: any) => {
      // console.log('Result from the fingerprint', result);
      res = true;
    }).catch((error: any) => {
      console.error('Error for fingerprint', error);
      res = false;
    });
    return res;
  }
}
