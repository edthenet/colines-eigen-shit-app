import { Injectable } from '@angular/core';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { AuthenticationService } from './auth';
import { User } from '../models/users';
import { Platform } from '@ionic/angular';
import { Device } from '@ionic-native/device/ngx';
import { StaticInformationProvider } from './static-information/static-information';
import { ApiProvider } from './api/api';

@Injectable({
  providedIn: 'root'
})
export class FirebaseFCMService {

  private myToken;
  private currentUser: User;

  constructor(
    public firebase: FirebaseX,
    public device: Device,
    public authSrv: AuthenticationService,
    private platform: Platform,
    private staticInfo: StaticInformationProvider,
    private api: ApiProvider,
  ) {
    this.authSrv.currentUser.subscribe(x => this.currentUser = x);
  }

  async getToken() {
    if (!this.myToken) {
      this.myToken = await this.firebase.getToken();

      // for iOS we also need to determine if the permission is set
      if (this.platform.is('ios')) {
        // const pushPermission = await this.firebase.hasPermission();
        // console.log('HASPERMISSION:', pushPermission.isEnabled);
        // if (!pushPermission.isEnabled) {
            await this.firebase.grantPermission();
        // }
      }
    }
    return this.myToken;
  }

  listenToNotifications() {
    // return this.firebase.onNotificationOpen();
    return this.firebase.onMessageReceived();
  }

  getMyToken() {
    return this.myToken;
  }

  registerDevice() {
    if (!this.myToken) {
      return;
    }

    const data = {
      userId: this.currentUser.id,
      pushToken: this.myToken,
      appId: this.staticInfo.getAppID(),
      manufacturer: this.device.manufacturer,
      model: this.device.model,
      platform: this.device.platform,
      platformVersion: this.device.version
    };

    // TODO: Voor optimalisatie kan ik hier nog een controle inbouwen of het device al op mijn naam staat
    // console.log(this.currentUser.user_devices);
    const udi = this.currentUser.user_devices.findIndex(uds => uds.pushToken === this.myToken);
    // if (udi === -1) {
    return this.api.registerDevice(data);
    // } else {
      // return the userdevice as observable
    // }
  }

}

