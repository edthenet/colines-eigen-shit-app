import { Injectable } from '@angular/core';
import { environment } from 'src/app/../environments/environment';

 @Injectable({
  providedIn: 'root'
})

export class LoggingService {

  private loglevels: string[] = ['log', 'info', 'warn', 'error'];

  constructor() { }

  public log(message, objectToDisplay?, logLevel?: string, forceShow?: boolean) {
    if (!logLevel) { logLevel = 'log'; }
    if (this.isMinimumLogLevel(logLevel, environment.minLogLevel) || forceShow) {
      console[logLevel](message, objectToDisplay ? objectToDisplay : '');
    }
  }

  public info(message, objectToDisplay?, forceShow?: boolean) {
    this.log(message, objectToDisplay, 'info', forceShow);
  }

  public warn(message, objectToDisplay?, forceShow?: boolean) {
    this.log(message, objectToDisplay, 'warn', forceShow);
  }

  public error(message, objectToDisplay?, forceShow?: boolean) {
    this.log(message, objectToDisplay, 'error', forceShow);
  }

  isMinimumLogLevel(ll, minll) {
    if (!ll || !minll) { return false; }
    const mll = this.loglevels.findIndex(mllx => mllx === minll);
    const ull = this.loglevels.findIndex(ullx => ullx === ll);
    return mll <= ull;
  }

}
