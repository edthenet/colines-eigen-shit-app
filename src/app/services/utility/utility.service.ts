import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor(
    public http: HttpClient,
    public toastController: ToastController,
    ) {
    // console.log('Hello UtilityService Provider');
  }

  async presentToast(msg: string, displayTime?, place?, callback?) {

    const toast = await this.toastController.create({
      message: msg,
      duration: displayTime ? displayTime : '5000',
      position: place ? place : 'bottom',
      keyboardClose: true,
      buttons: displayTime >= 9999 ? [{
        text: '',
        icon: 'close-circle-outline',
        side: 'end',
        cssClass: 'custom-toast-error-button'
      }] : []
    });
    toast.present();
    if (callback) {
      toast.onDidDismiss().then(callback);
    }
  }

  getAvatarURL(userData) {
    if (userData.avatar) {
      if (userData.avatar.startsWith('data')) {
        return userData.avatar;
      }
      if (userData.avatar.substr( userData.avatar.length - 3 ) !== 'jpg' && userData.avatar.substr( userData.avatar.length - 3 ) !== 'png' ) {
        return 'data:image/png;base64,' + userData.avatar;
      } else {
        return environment.serverUrl + '/' + environment.api_version + '/files/free/avatars/' + userData.avatar ;
      }
    } else {
      return 'assets/imgs/profile_image.jpg';
    }
  }

  imageExists(imageUrl) {
    const http = new XMLHttpRequest();
    http.open('HEAD', imageUrl, false);
    http.send();
    return http.status === 200;
  }

}
