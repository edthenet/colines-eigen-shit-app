import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/auth/auth.guard';

const routes: Routes = [
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule', canActivate: [AuthGuard] },
  { path: 'shit-images-detail/:id', loadChildren: './pages/shit-images-detail/shit-images-detail.module#ShitImagesDetailPageModule', canActivate: [AuthGuard]  },
  { path: 'userprofile', loadChildren: './pages/profile/profile.module#ProfilePageModule', canActivate: [AuthGuard]  },
  { path: 'users', loadChildren: './pages/users/users.module#UsersPageModule', canActivate: [AuthGuard]  },
  { path: 'user-detail/:id', loadChildren: './pages/users/user-detail/user-detail.module#UserDetailPageModule', canActivate: [AuthGuard]  },
  { path: 'info-tabs', loadChildren: './pages/info-tabs/info-tabs.module#InfoTabsPageModule', canActivate: [AuthGuard]  },
  { path: 'info-page/:label', loadChildren: './pages/info-tabs/info-page/info-page.module#InfoPagePageModule', canActivate: [AuthGuard]  },
  { path: 'about-scorpion', loadChildren: './pages/about-scorpion/about-scorpion.module#AboutScorpionPageModule', canActivate: [AuthGuard]  },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: '/login' }
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    {
      enableTracing: false, // <-- debugging purposes only
      // preloadingStrategy: SelectivePreloadingStrategyService,
    }
    )],
  exports: [RouterModule]
})
export class AppRoutingModule {}
