export class ShitImage {
  id: number;
  senderId: number;
  title: string;
  text: string;
  url: string;
  createdAt: string;
  updatedAt: string;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
