export class JoomlaArticles {
    status: string;
    total: number;
    limit: number;
    offset: number;
    pages_current: number;
    pages_total: number;
    articles: JoomlaArticle[];

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

export class JoomlaArticle {
    id: string;
    title: string;
    alias: string;
    featured: string;
    content: string;
    catid: string;
    images: [{
        float_fulltext: string;
        float_intro: string;
        image_fulltext: string;
        image_fulltext_alt: string;
        image_fulltext_caption: string;
        image_intro: string;
        image_intro_alt: string;
        image_intro_caption: string;
    }];
    tags: [{
        id: string;
        title: string;
        alias: string;
        language: string;
    }];
    fields: [{
        id: string;
        title: string;
        name: string;
        type: string;
        default_value: string;
        group_id: string;
        label: string;
        description: string;
        required: string;
        value: string;
        rawvalue: string;
    }];
    metakey: string;
    metadesc: string;
    metadata: [{
        robots: string;
        author: string;
        rights: string;
        xreference: string;
    }];
    language: string;
    category_title: string;
    category_alias: string;
    author: string;
    created_date: string;
    modified_date: string;
    published_date: string;
    unpublished_date: string;
    state: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
