export class UserDevice {

  id: number;
  userId: number;
  pushToken: string;
  appId: string;
  manufacturer: string;
  model: string;
  platform: string;
  platformVersion: string;
  createdAt: string;
  updatedAt: string;

  constructor(values: object = {}) {
    Object.assign(this, values);
  }
}
