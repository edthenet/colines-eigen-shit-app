export class BasicContent {
    label: string;
    title: string;
    content: string;
    createdAt: string;
    updatedAt: string;

    constructor(values: Object = {}) {
      Object.assign(this, values);
    }
  }
