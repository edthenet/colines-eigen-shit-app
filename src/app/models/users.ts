import { UserDevice } from './userdevices';

export class User {
    id: number;
    firstname: string;
    lastname: string;
    company: string;
    emailPrivate: string;
    emailBusiness: string;
    password: string;
    phonePrivate: string;
    phoneBusiness: string;
    avatar: string;
    isActive: boolean;
    role: string;
    note: string;
    resetPasswordToken: string;
    createdAt: string;
    updatesAt: string;
    deletedAt: string;
    latestLogin: string;
    token?: string;
    user_devices: UserDevice[];

    constructor(values: Object = {}) {
      Object.assign(this, values);
    }
  }
