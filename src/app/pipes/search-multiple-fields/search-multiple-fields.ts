// Example usage: <li *ngFor="let n of list | searchMultipleFields: queryString : ['name','age'] ">
// Or you can also set this.searchableFields = ['name','age'] in the module
// and use: <li *ngFor="let n of list | searchMultipleFields: queryString : searchableFields ">
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchMultipleFields',
})
export class SearchMultipleFieldsPipe implements PipeTransform {
  transform(value: any, input: string, searchableFields: any) {
    if (input) {
      input = input.toLowerCase();
      return value.filter((el: any) => {
        var isTrue = false;
        for (let searchAbleField of searchableFields) {

          let fieldArray = searchAbleField.split('.');

          let element = el;
          for (let fieldPart of fieldArray) {
            element = element[fieldPart];
          }

          let inputs = input.split(' ');

          for (let i of inputs) {
            if(i != ""){
              switch (typeof element) {
                case "string":
                  if (element.toLowerCase().indexOf(i) > -1) {
                    isTrue = true;
                  }
                  break;
                case "number":
                  if (element.toString().indexOf(i) > -1) {
                    isTrue = true;
                  }
                  break;
              }
            }
          }

          if (isTrue) {
            return el
          }
        }
      })
    }
    return value;
  }
}
