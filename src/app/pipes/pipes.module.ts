import { NgModule } from '@angular/core';
import { SearchMultipleFieldsPipe } from './search-multiple-fields/search-multiple-fields';

@NgModule({
	declarations: [SearchMultipleFieldsPipe],
	imports: [],
	exports: [SearchMultipleFieldsPipe]
})
export class PipesModule {}
