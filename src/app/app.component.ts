import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { User } from './models/users';
import { AuthenticationService } from './services/auth';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { StaticInformationProvider } from './services/static-information/static-information';
import { UtilityService } from './services/utility/utility.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  appVersion: string;
  appName: string;
  currentUser: User;

  public appPages = [
    { title: 'Allerlei shit', icon: 'aperture', userRole: ['manager', 'admin', 'user'], url: '/home'},
    // { title: 'Berichten', icon: 'mail', userRole: ['manager', 'admin'], url: ''},
    { title: 'Informatie', icon: 'information-circle', userRole: ['user', 'manager', 'admin'], url: '/info-tabs' },
  ];

  constructor(
    public authService: AuthenticationService,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateService,
    private router: Router,
    private staticInfo: StaticInformationProvider,
    public utilitySrv: UtilityService,
    // private menuCtrl: MenuController,
  ) {
    this.authService.currentUser.subscribe(x => this.currentUser = x);
    this.appName = this.staticInfo.getAppName();
    this.appVersion = this.staticInfo.getAppVersion();

    // this.menuCtrl.enable(false, 'mainMenu');
    this.initTranslate();
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // this.statusBar.styleDefault();
      // this.splashScreen.hide();
    });
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    // this.translate.addLangs(["nl", "en"]);
    this.translate.setDefaultLang('nl');
    const browserLang = this.translate.getBrowserLang();

    if (browserLang) {
      console.log('De browser language:', browserLang);
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use('nl'); // Set your language here
    }
  }

  getAvatarURL() {
    if (this.currentUser && this.currentUser.avatar && this.currentUser.avatar.length > 1) {
      return environment.serverUrl + '/' +
        environment.api_version + '/files/avatars/' +
        this.currentUser.avatar + '?auth=' +
        this.authService.currentUserValue.token;
    } else {
      // return '/assets/imgs/profile_image.png';
      return '/assets/imgs/logo.png';
    }
  }

  openPageUserAccount() {
    this.router.navigate(['/userprofile']);
  }
}
