import { NgModule } from '@angular/core';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { Dialogs } from '@ionic-native/dialogs/ngx';

import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { TranslateModule, TranslateLoader, TranslatePipe } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { ErrorInterceptor, JwtInterceptor } from './services/interceptors';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { Device } from '@ionic-native/device/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { OrderModule } from 'ngx-order-pipe';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

// Laad taalfunctionaliteit voordat de app gestart word
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    // IonicModule.forRoot(MyApp, {
    //   backButtonText: 'Terug',
    //   iconMode: 'ios',
    //   modalEnter: 'modal-slide-in',
    //   modalLeave: 'modal-slide-out',
    //   tabsPlacement: 'top',
    //   pageTransition: 'ios-transition',
    //   links: [
    //     { component: ForgotPasswordPage, name: 'Forgot password', segment: 'forgot-password' }
    //   ],
    // }),
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    OrderModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Dialogs,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    FirebaseX,
    LocalNotifications,
    Device,
    FingerprintAIO,
  ],
  exports: [
    TranslateModule,
    TranslatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
