export const environment = {
  production: true,
  mode: 'Production',
  serverUrl: 'http://api.scorpionsoftware.nl:3000',
  api_version: 'api/v1',
  minLogLevel: 'info',
  minrole: 'user',
};
