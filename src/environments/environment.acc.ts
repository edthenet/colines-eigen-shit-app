export const environment = {
  production: false,
  mode: 'Acceptation',
  serverUrl: 'http://api-acc.scorpionsoftware.nl:9000',
  api_version: 'api/v1',
  minLogLevel: 'info',
  minrole: 'user',
};
