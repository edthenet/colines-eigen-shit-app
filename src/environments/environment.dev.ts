export const environment = {
  production: false,
  mode: 'Development',
  serverUrl: 'http://localhost:3000',
  api_version: 'api/v1',
  minLogLevel: 'info',
  minrole: 'user',
};
