# Releasenotes

## Versie 3.1.0 - xx-09-2020

- Update naar Angular 10
- Update naar Ionic 5
- Update Firebase naar FirebaseX
- Toevoeging van diverse statische settings
- Toevoeging van een logging service

## Versie 3.0.1 - 11-10-2019

- Update naar Angular 8
- Update naar nieuwe Android target versie

## Versie 3.0.0 - 11-10-2019

- Omgebouwd naar Ionic 4 / Angular 7
- Push notificatie toegevoegd
- Aangepaste styling
- Aangepaste iconen
- Enkel toegang voor geregistreerde gebruikers i.v.m. gebruikers content in de nabije toekomst
- Login en registratie toegevoegd
- Informatie over app en Scorpion toegevoegd
- Changelog voor versie informatie toegevoegd
- Inlog mogelijkheid toegevoegd met TouchID of FaceID
- Toevoegen van shit Images (funnies) mogelijk gemaakt (Nu nog enkel via invoer van een url)

## Versie 2.0.2 - 04-01-2019

- Aangepast adres voor Scorpion Computers & Software
- Nieuwe iconen voor compatibiliteit met iOS en Android en vernieuwde stijl
- Changelog toegevoegd in separaat bestand

## Versie 2.0.1

- Geschikt gemaakt voor de nieuwste versie van het Android Platform

## Versie 2.0.0

- Implementatie van Ionic en bijbehorende functionaliteit en verbeteringen
- Aangezien deze informatie pas is bijgehouden vanaf versie 2.0.0 zal er weinig bekend zijn over historische versies

## Versie 1.0.0

- Oorspronkelijke POC uitgebreacht. Gemaakt met Angular
