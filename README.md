# Coline's Eigen Shit App #

## Geschiedenis ##

De ontwikkelaar van deze app (Edwin Witlox) deelt, met zijn zus Coline, altijd allerlei onzin en plaatjes die te maken hebben met het onderwerp "poep". Afbeeldingen, cartoons en filmpjes over scheten en poep vinden ze hylarisch. Edwin moest een proof of concept maken om informatie vanuit een op Joomla! gebasseerde website te presenteren in een mobiele app. Dit bleek het moment om de POC ook van een grappige toets te voorzien. Aangezien Coline een poosje niet mobiel was vond Edwin het wel een leuk idee om Coline weer een beetje mobiel te maken door een echte App, speciaal voor haar, te ontwikkelen.

## In de appstores ##

Aangezien meerdere mensen deze app wel grappig vonden ging deze zijn eigen leven leiden. De app moest dus ook worden verspreid. Niet alleen naar Android devices waarbij het gemakkelijk was om een link op te sturen, maar ook naar iOS apparaten. De app is dus officieel in de appstores beland.

## Functionaliteit ##

Op dit moment heeft de app alleen maar als functionaliteit het weergeven van poep-gerelateerde artikelen vanaf een Joomla! website.
Natuurlijk is er een pagina gemaakt met informatie over de app en een met informatie over Scorpion Computers & Software.

## Contactinformatie ##

Naam | E-mail | Telefoon | Rol
--- | --- | --- | ---
Edwin Witlox | edwin@scorpioncomputers.nl | +31-6-55321492 | Projectmanager

## Om te beginnen ##

- Haal de GIT repo op
- `npm install`
- `npm install -g cordova ionic`
- Start de API Gateway (zie readme.md van het desbetreffende project)
- `ionic serve` of `npm start`

------

## Gebruikte plugins ##

- [Angular 4+ Order Pipe](https://github.com/VadimDez/ngx-order-pipe) wordt gebruikt om de | orderBy functionaliet toe te voegen aan de Angular applicatie (werkt als in AngularJS, maar maakt gebruik van een pipe)
- [Ionic Native Secure Storage](https://ionicframework.com/docs/native/secure-storage/) om gebruikersgegevens op een veilige manier op te slaan op het device
- [Ionic Native Device](https://ionicframework.com/docs/native/device/) Om gegevens te verkrijgen van het device en het OS
- [Angular2 JWT](https://www.npmjs.com/package/angular2-jwt) Gebruikt voor oAuth2
- [Angular Editor](https://www.npmjs.com/package/@kolkov/angular-editor) WYSIWYG editor voor bepaalde content
- [NGX-Translate](https://ionicframework.com/docs/v3/developer-resources/ng2-translate/) wordt gebruikt voor meertaligehid binnen de applicatie. Belangrijk is om wel de juiste versie te installeren die verbonden is aan je angular versie. Meer info hier hierover vind je op de [Github pagina van ngx-translate](https://github.com/ngx-translate/core).
- [Ionic Native Dialogs](https://ionicframework.com/docs/native/dialogs/) Om een bevestigings dialog te tonen voor het verwijderen van data
- [Ionic Native Firebase](https://ionicframework.com/docs/native/firebase) voor het verwerken van pushnotificaties
- [Ionic Native Contacs](https://ionicframework.com/docs/native/contacts) om de contactgegevens van Scorpion direct op te slaan in de contacten van de gebruiker
- [Ionic Storage](https://ionicframework.com/docs/building/storage) om settings lokaal op te slaan
- [Ionic Native Fingerprint AIO](https://ionicframework.com/docs/native/fingerprint-aio) om in te loggen met fingerprint of faceID
- [Ionic Native Dialogs](https://ionicframework.com/docs/native/dialogs) om dialoogschermpje te tonen om bv. vragen te stellen aan een gebruiker
- [VadimDez Order Pipe](https://github.com/VadimDez/ngx-order-pipe) Om lijstjes te sorteren

## Informatie voor developers ##

- Push icoon: Wanneer het android platform word weggehaald (remove platform) en weer terug wordt gezet dienen de iconen voor push opnieuw te worden gekopieerd. Instructies hiervan zijn te vinden op [Deze website](https://documentation.onesignal.com/docs/customize-notification-icons) en de iconen vind je in de _Graphics folder onder de naam `ßApeToolsGeneratedAssets.zip`

## Toekomst ##

Deze app zal worden gebruikt voor verdere experimenten en proof-of-concepts, maar omdat er toch wel een aantal gebruikers zijn van deze app zullen we de app ook daadwerkelijk moeten onderhouden. We hebben nog wel plannen voor toekomstige versies.

- Nieuwe versie zal worden ontwikkeld in Ionic 3+ om ervoor te zorgen dat de gebruikerservaring platform onafhankelijk wordt en de mogelijkheid ontstaat voor een Windows versie.
- Push notificatie zal worden geimplementeerd om aan te geven wanneer er nieuwe berichten zijn.
- Mensen moeten zelf content kunnen aanleveren. Deze zal pas na goedkeuring worden getoond.
- Contactgegevens van Scorpion Computers & Software kan automatisch worden toegevoegd aan de contactpersonen
- Pull to refresh implementatie
- iOS 11 en hoger en ook iPhone X ondersteuning

## Even onthouden ##

in config.xml
Dit moet voor fingerprint AIO
    <preference name="UseSwiftLanguageVersion" value="4.0" />

